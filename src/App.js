import Header from "./components/Header/Header.js";
import Modal from "./components/Modal/Modal.js";
import Button from "./components/Button/Button.js";
import AppRoutes from "./routes/AppRoutes.js";
import Footer from "./components/Footer/Footer.js";
import React, { useState, useEffect } from "react";
import { AppWrapper, BtnsWrapper } from "./App-styles.js";
import axios from "axios";

const App = () => {
  const [showPurchaseModal, setPurchaseModal] = useState(false);
  const [showDeleteModal, setDeleteModal] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [goods, setGoods] = useState([]);
  const [cart, setCart] = useState(
    JSON.parse(localStorage.getItem("Cart")) || []
  );
  const [favorites, setFavorites] = useState(
    JSON.parse(localStorage.getItem("Favorites")) || []
  );

  const switchDeleteModal = () => {
    setDeleteModal(!showDeleteModal);
  };

  const switchPurchaseModal = () => {
    setPurchaseModal(!showPurchaseModal);
  };

  useEffect(() => {
    axios("/goods.json").then((res) => {
      setGoods(res.data);
      setIsLoading(!isLoading);
    });
  }, []);

  const addToCart = () => {
    const currentCard = document.querySelector(".activeCard");
    const id = currentCard.id;

    const sameValue = cart.find((el) => el === Number(id));

    const cartValue = sameValue
      ? cart.filter((el) => el !== sameValue)
      : [...cart, Number(id)];

    setCart(cartValue);

    localStorage.setItem("Cart", JSON.stringify(cartValue));

    currentCard.classList.remove("activeCard");

    setPurchaseModal(false);
    setDeleteModal(false);
  };

  const addToFavorites = (id) => {
    const sameValue = favorites.find((el) => el === id);

    const favoriteValue = sameValue
      ? favorites.filter((el) => el !== sameValue)
      : [...favorites, Number(id)];

    setFavorites(favoriteValue);

    localStorage.setItem("Favorites", JSON.stringify(favoriteValue));
  };

  const createBtns = (
    text1,
    text2,
    bgColor1,
    bgColor2,
    padding1,
    padding2,
    handleClick1,
    handleClick2
  ) => {
    return (
      <BtnsWrapper>
        <Button
          padding={padding1}
          bgColor={bgColor1}
          margin={"0 15px 0 0"}
          id={"confirmationBtn"}
          text={text1}
          handleClick={handleClick1}
        />
        <Button
          padding={padding2}
          bgColor={bgColor2}
          id={"refusalBtn"}
          text={text2}
          handleClick={handleClick2}
        />
      </BtnsWrapper>
    );
  };

  const purchaseModalBtns = createBtns(
    "Да",
    "Нет",
    "#6666ff",
    "#ff0000",
    "11px 20px 14px",
    "11px 20px 14px",
    addToCart,
    switchPurchaseModal
  );

  const deleteModalBtns = createBtns(
    "Да",
    "Нет",
    "#B20000",
    "#0039b2",
    "11px 20px 14px",
    "11px 20px 14px",
    addToCart,
    switchDeleteModal
  );

  const purchaseModal = (
    <Modal
      closeBtn={false}
      title={"Подтверждение"}
      show={showPurchaseModal}
      id="purchase_modal"
      handleClick={switchPurchaseModal}
      bgColor={"#fff"}
      bgHeaderColor={"#6666ff"}
      btns={purchaseModalBtns}
      text={"Вы хотите добавить товар в корзины?"}
    />
  );

  const delModal = (
    <Modal
      closeBtn={false}
      title={"Вы уверены?"}
      show={showPurchaseModal}
      id="delete_modal"
      handleClick={switchDeleteModal}
      bgColor={"#fff"}
      bgHeaderColor={"#B20000"}
      btns={deleteModalBtns}
      text={"Вы хотите удалить товар из корзины?"}
    />
  );

  return (
    <>
      <AppWrapper>
        <Header />
        {!isLoading && (
          <AppRoutes
            switchPurchaseModal={switchPurchaseModal}
            switchDeleteModal={switchDeleteModal}
            addToFavorites={addToFavorites}
            goods={goods}
            favorites={favorites}
            cart={cart}
          />
        )}
        {showPurchaseModal && purchaseModal}
        {showDeleteModal && delModal}
        <Footer />
      </AppWrapper>
    </>
  );
};

export default App;
