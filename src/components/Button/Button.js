import React from "react";
import { StyledButton } from "./Button-styles.js";
import PropTypes from "prop-types";

const Button = ({ text, handleClick, className, id, ...rest }) => {
  return (
    <StyledButton className={className} id={id} {...rest} onClick={handleClick}>
      {text}
    </StyledButton>
  );
};

export default Button;

Button.propsTypes = {
  text: PropTypes.string,
  handleClick: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  className: PropTypes.string,
};

Button.defaultProps = {
  text: "Купить",
};
