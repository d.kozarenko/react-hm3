import React from "react";
import Product from "../../components/Product/Product";
import {
  FavoritesWrapper,
  FavoritesMessage,
  FavoritesList,
} from "./Favorites-styles";

const Favorites = ({
  cart,
  favorites,
  goods,
  switchPurchaseModal,
  addToFavorites,
}) => {
  const favoritesArr = [];

  goods.forEach((f) => {
    if (favorites.includes(f.id)) {
      favoritesArr.push(f);
    }
  });

  const favoritesList = favoritesArr.map((f) => (
    <Product
      handleClick={switchPurchaseModal}
      addToFavorites={addToFavorites}
      favorites={favorites}
      cart={cart}
      id={f.id}
      key={f.id}
      product={f}
    />
  ));

  return (
    <FavoritesWrapper>
      {favorites.length === 0 && (
        <FavoritesMessage>
          Вы пока не добавили ничего в избранное
        </FavoritesMessage>
      )}
      {favorites.length !== 0 && <FavoritesList>{favoritesList}</FavoritesList>}
    </FavoritesWrapper>
  );
};

export default Favorites;
